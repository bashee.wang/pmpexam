from django import template


register = template.Library()


def multiply(value, arg):
    if value == 0:
        return 0
    else:
        return int(value) * int(arg)


def divided(value, arg):
    return int(value) % int(arg)


register.filter('multiply', multiply)
register.filter('divided', divided)
