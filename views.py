from django.shortcuts import render, get_object_or_404
# from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.views import generic
from django.urls import reverse
from .models import Question, Options
from .forms import AddQuestionForm, AddOptionsForm


# Create your views here.
# def index(request):
#     return HttpResponse("HI")


class IndexView(generic.ListView):
    template_name = 'pmpexam/index.html'
    context_object_name = 'category_list'

    def get_queryset(self):
        return Question.objects.values_list('category').distinct()

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['agent'] = self.request.META['HTTP_USER_AGENT']
        if Question.objects.count() > 0:
            context['first_index'] = Question.objects.values_list('id')[0][0]
        else:
            context['first_index'] = 0
        cate_all = [q[0] for q in Question.objects.values_list('category')]
        context['cate_data'] = {x: cate_all.count(x) for x in cate_all}
        return context


class QView(generic.DetailView):
    template_name = 'pmpexam/detail.html'
    model = Question

    def get_context_data(self, **kwargs):
        context = super(QView, self).get_context_data(**kwargs)
        context['question_number'] = Question.objects.count()
        id_query = [q[0] for q in Question.objects.values_list('id')]
        pk_index = id_query.index(int(self.kwargs['pk']))
        if pk_index + 1 < Question.objects.count():
            context['next_id'] = id_query[pk_index + 1]
        return context


class CateView(generic.ListView):
    template_name = 'pmpexam/cate.html'
    context_object_name = 'question_list'
    slug_field = 'category'

    def get_queryset(self):
        return Question.objects.filter(category__exact=self.kwargs['slug'])

    def get_context_data(self, **kwargs):
        context = super(CateView, self).get_context_data(**kwargs)
        q_value = [id[0] for id in Question.objects.filter(
            category__exact=self.kwargs['slug']).values_list('id')]
        q_key = list(range(1, len(q_value) + 1))
        context['q_no'] = dict(zip(q_key, q_value))
        context['cate'] = self.kwargs['slug']
        return context


class CQView(generic.DetailView):
    template_name = 'pmpexam/cqdetail.html'
    model = Question

    def get_context_data(self, **kwargs):
        context = super(CQView, self).get_context_data(**kwargs)
        context['cate'] = self.kwargs['category']
        id_query = [q[0] for q in Question.objects.filter(
            category__exact=self.kwargs['category']).values_list('id')]
        context['question_id_list'] = id_query
        pk_index = id_query.index(int(self.kwargs['pk']))
        if pk_index + 1 < Question.objects.filter(
                category__exact=self.kwargs['category']).count():
            context['next_id'] = id_query[pk_index + 1]
        return context


class RView(generic.DetailView):
    template_name = 'pmpexam/result.html'
    model = Question

    def get_context_data(self, **kwargs):
        context = super(RView, self).get_context_data(**kwargs)
        context['question_number'] = Question.objects.count()
        id_query = [q[0] for q in Question.objects.values_list('id')]
        pk_index = id_query.index(int(self.kwargs['pk']))
        if pk_index + 1 < Question.objects.count():
            context['next_id'] = id_query[pk_index + 1]
        return context


# Edit a New Question
def edit_question(request, pk):
    q_edit = get_object_or_404(Question, id=pk)
    if request.method == "POST":
        qform = AddQuestionForm(request.POST, instance=q_edit)
        if qform.is_valid():
            qpost = qform.save(commit=False)
            qpost.save()
            return HttpResponseRedirect(
                reverse('pmp:result', args=[q_edit.id, ]))
    else:
        qform = AddQuestionForm(instance=q_edit)
    return render(
        request,
        'pmpexam/add_new.html',
        {
            'qform': qform,
        }
    )


# Add a New Question
def add_new(request):
    options_index = ['A', 'B', 'C', 'D']
    if request.method == "POST":
        qform = AddQuestionForm(request.POST)
        oforms = [AddOptionsForm(request.POST, prefix=x, instance=Options())
                  for x in options_index]
        if qform.is_valid() and all([of.is_valid() for of in oforms]):
            qpost = qform.save(commit=False)
            qpost.save()
            for of in oforms:
                opost = of.save(commit=False)
                opost.question = qpost
                opost.save()
            return HttpResponseRedirect(
                reverse('pmp:index', args=[]))
    else:
        qform = AddQuestionForm(instance=Question())
        oforms = [AddOptionsForm(prefix=x, instance=Options())
                  for x in options_index]
    return render(
        request,
        'pmpexam/add_new.html',
        {
            'qform': qform,
            'oforms': oforms,
        }
    )


# choose an option
def choose(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        chosen_option = question.options_set.get(pk=request.POST['option'])
    except (KeyError, Options.DoesNotExist):
        return render(
            request,
            'pmpexam/detail.html',
            {
                'question': question,
                'error_message': "No option selected.",
            }
        )
    else:
        chosen_option.votes += 1
        chosen_option.save()
        return HttpResponseRedirect(
            reverse('pmp:result', args=[question.id, ]))
