from django.db import models
from tinymce import HTMLField


# Create your models here.
# Table 1: Question:
class Question(models.Model):
    # 1. Question Text
    # question_text = models.TextField(
    #     'Question subject',
    #     max_length=500,
    # )
    question_text = HTMLField('Question Subject')

    def __str__(self):
        return self.question_text

    # 2. Answer to Question
    ANSWERS = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
    )
    answer = models.CharField(
        'The answer to the question',
        max_length=1,
        choices=ANSWERS,
        null=True, blank=True
    )

    # 3. which chapter this question belong 2
    chapter = models.CharField(
        'Question belong to Which chapter',
        max_length=2,
        null=True, blank=True
    )

    # 4. How to analysis this question
    description = models.TextField(
        'The analysis for this question.',
        max_length=1000,
        null=True, blank=True
    )

    # 5. This question has been learned or not
    leanred = models.BooleanField(
        'Mark it as learned.',
        default=False
    )

    # 6. Question Category
    category = models.CharField(
        'Where this question come from?',
        max_length=20,
        null=True,
        blank=True,
        default=''
    )


# Table 2: Options:
class Options(models.Model):
    # 1. Foreign Key
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    # 2.0 Options index to this question
    OPTION_INDEX = (
        ('A', 'A'),
        ('B', 'B'),
        ('C', 'C'),
        ('D', 'D'),
    )
    option_index = models.CharField(
        'Options to the question',
        max_length=1,
        choices=OPTION_INDEX,
        default='A'
    )
    # 2.1 Options to this question
    option_text = models.CharField(
        'Options to the question',
        max_length=100,
    )

    def __str__(self):
        return self.option_index + '. ' + self.option_text

    # 3. How many this option was selected.
    votes = models.IntegerField(default=0)
