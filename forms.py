from django import forms
from .models import Question, Options


class AddQuestionForm(forms.ModelForm):

    class Meta:
        model = Question
        fields = (
            'question_text',
            'answer',
            'chapter',
            'leanred',
            'description',
            'category',
        )


class AddOptionsForm(forms.ModelForm):

    class Meta:
        model = Options
        fields = (
            # 'question',
            'option_index',
            'option_text',
        )
