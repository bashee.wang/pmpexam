from django.conf.urls import url
from . import views


app_name = 'pmp'
urlpatterns = [
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^cat/(?P<slug>\w+)/$', views.CateView.as_view(), name='cate'),
    url(r'^cat/(?P<category>\w+)/question/(?P<pk>\d+)/$',
        views.CQView.as_view(), name='cqdetail'),
    url(r'^new/$', views.add_new, name='add_new'),
    url(r'^question/(?P<pk>\d+)/edit/$', views.edit_question, name='edit'),
    url(r'^question/(?P<pk>\d+)/$', views.QView.as_view(), name='detail'),
    url(r'^question/(?P<pk>\d+)/result/$', views.RView.as_view(), name='result'),
    url(r'^question/(?P<question_id>\d+)/choose/$', views.choose, name='choose'),
]
