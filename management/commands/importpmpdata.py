from django.core.management.base import BaseCommand
from pmpexam.models import Question
import re
import os
# import argparse
# import ipdb


class Command(BaseCommand):
    help = 'Import PMP data to DB'

    def add_arguments(self, parser):
        parser.add_argument('infile', nargs='?')
        parser.add_argument('category', nargs='?', default='uncategoried')

    def handle(self, *args, **options):
        # ipdb.set_trace()
        with open(options['infile'], 'r') as inputfile:
            i = 0
            for line in inputfile:
                l = line.rstrip(os.linesep)
                myq = re.match('^\s?\d+\.(.*)$', l)
                myo = re.match('^\s?([A-Da-d])\.(.*)$', l)
                if myq or myo:
                    if myq:
                        i += 1
                        q = Question(question_text=myq.group(1),
                                     category=options['category'])
                        q.save()
                        print("Import recored: ", i, "...")
                    if myo:
                        q.options_set.create(option_index=myo.group(
                            1).upper(), option_text=myo.group(2))
