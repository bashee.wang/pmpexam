from django.contrib import admin
from .models import Question, Options


class OptionsInLine(admin.TabularInline):
    model = Options
    extra = 4


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {
            'fields': [
                'question_text',
                'chapter',
                'answer',
                'description',
                'leanred',
                'category',
            ]}),
    ]
    inlines = [OptionsInLine]
    list_filter = ['chapter']
    search_field = ['question_text']


# Register your models here.
admin.site.register(Question, QuestionAdmin)
admin.site.register(Options)
