# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-18 14:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pmpexam', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='question',
            name='description',
            field=models.CharField(blank=True, max_length=1000, null=True, verbose_name='The analysis for this question.'),
        ),
    ]
